#!/bin/bash

# Plays Bad Apple on the shape of windows of a certain classname
# Requires: yt-dlp, ffmpeg, xprop
# Needs better synchronization with the audio, change SYNC if necessary

URL=https://www.youtube.com/watch?v=G3C-VevI36s
VIDEO=video.webm
FOLDER=frames
FRAMES=6570
SYNC=120

[ -f "$VIDEO" ] || yt-dlp "$URL" -o "$VIDEO"

[ -d "$FOLDER" ] || mkdir -p "$FOLDER"
[ -f "$FOLDER/$FRAMES.xbm" ] || ffmpeg -i "$VIDEO" "$FOLDER/%04d.xbm"

WM_NAME="$(xprop | awk '/WM_CLASS/{print $3}')"
WM_NAME="${WM_NAME:1:-2}"

i=1
diff=0
ffplay -vn -nodisp -loglevel 8 -stats "$VIDEO" 2>&1 | tr '\r' '\n' \
| while true; do
    read -r line
    now="$(echo "$line" | cut -d' ' -f1)"
    frame="$(echo "30 * ($now + $diff)" | bc -l)"

    printf "BadApple\0window\0name\0$WM_NAME\0mask\0$FOLDER/%04d.xbm\0\0" \
           "$(echo "($frame + 0.5)/1 + $SYNC" | bc)"
done | ./misaki
