# Misaki

A compositor for X11

## Installation
Needs a C compiler, a POSIX system, make, and the following libraries:
- libx11
- libXext
- libxdamage
- libxrender
- libxcomposite
- libxpm
```
make
sudo make install
```
