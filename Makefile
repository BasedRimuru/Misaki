# This file is part of Misaki.
#
# Misaki is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published
# by the Free Software Foundation, version 3.
#
# Misaki is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Misaki. If not, see <https://www.gnu.org/licenses/>.

CFLAGS += -O2 -std=gnu99 -DNDEBUG -Wall -Wextra
CFLAGS += -Wno-unused-function -Wno-unused-parameter
CFLAGS += -Ideps/Index/include
LDFLAGS += -lX11 -lXext -lXrender -lXdamage -lXcomposite -lXpm

all: misaki

install:
	cp misaki "$(DESTDIR)/usr/bin"
uninstall:
	rm -f "$(DESTDIR)/usr/bin/misaki"

clean:
	rm -f misaki
depclean:
	rm -rf deps

deps:
	mkdir deps
deps/Index/include: | deps
	cd deps && git clone https://gitlab.com/BasedRimuru/Index

misaki: main.c deps/Index/include
	gcc $(CFLAGS) $< $(LDFLAGS) -g -o $@
