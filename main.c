/*
This file is part of Misaki.

Misaki is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published
by the Free Software Foundation, version 3.

Misaki is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Misaki. If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>

#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/select.h>

#include <X11/Xlib.h>
#include <X11/Xatom.h>

#include <X11/extensions/shape.h>
#include <X11/extensions/Xdamage.h>
#include <X11/extensions/Xrender.h>
#include <X11/extensions/Xcomposite.h>

#include <X11/xpm.h>

/* Type definitions */

#define X_TYPE char
#define X_NAMING(x) char_vec_##x
#include "structure/vector.def"
#undef X_TYPE
#undef X_NAMING

#define X_TYPE char *
#define X_NAMING(x) charptr_vec_##x
#include "structure/vector.def"
#undef X_TYPE
#undef X_NAMING

#define X_TYPE Window
#define X_NAMING(x) window_vec_##x
#include "structure/vector.def"
#undef X_TYPE
#undef X_NAMING

struct winrule
{
    struct char_vec_t *id;

    Picture mask;
    uint mask_w;
    uint mask_h;

    Picture bg;
    Picture bgm;
    uint bg_w;
    uint bg_h;

    Picture fg;
    Picture fgm;
    uint fg_w;
    uint fg_h;
};

static size_t
winrule_map_hash(struct winrule *r)
{
    size_t ret = 5381;

    if (r != NULL && r->id != NULL)
    {
        for (size_t i = 0; i < r->id->count; i++)
            ret = ((ret << 5) + ret) + r->id->data[0];
    }

    return ret;
}

static bool
winrule_map_test(struct winrule *r, struct winrule *r2)
{
    bool ret = false;

    if (r != NULL && r2 != NULL && r->id != NULL && r2->id != NULL)
    {
        if (r->id->count == r2->id->count)
        {
            if (memcmp(r->id->data, r2->id->data, r->id->count) == 0)
                ret = true;
        }
    }

    return ret;
}

#define X_TYPE struct winrule
#define X_NAMING(x) winrule_map_##x
#include "structure/mapping.def"
#undef X_TYPE
#undef X_NAMING

enum status
{
    STATUS_OK,
    STATUS_ERROR
};

struct misaki
{
    Display *display;
    int screen;
    Window root;

    int width;
    int height;
    int depth;

    Visual *visual;
    XRenderPictFormat *format;

    XRenderPictFormat *rgb;
    XRenderPictFormat *a1;

    Picture wallpaper;
    Picture buffer;
    Picture buffer2;

    int xshape_ev;
    int xshape_err;
    int xdamage_ev;
    int xdamage_err;
    int xrender_ev;
    int xrender_err;
    int xcomposite_op;
    int xcomposite_ev;
    int xcomposite_err;

    struct window_vec_t *windows;

    int stopper[2];

    char *socket;
    struct char_vec_t *line;
    char *caller;
    struct charptr_vec_t *args;
    size_t argidx;
    char *response;

    struct winrule effects;
    struct winrule_map_t *name_effects;
    struct winrule_map_t *class_effects;
};

/* Helper functions */

static int
pstrcmp(char *str, char *str2)
{
    int ret = 0;

    if (str != NULL && str2 != NULL)
    {
        ret = strcmp(str, str2);
    }
    else if (str == NULL && str2 != NULL)
    {
        ret = -1;
    }
    else if (str != NULL && str2 == NULL)
    {
        ret = 1;
    }

    return ret;
}

/* Logging functions */

static enum status
error(char *string)
{
    fprintf(stderr, "misaki: %s\n", string);
    return STATUS_ERROR;
}

/* Windows effects functions */

static enum status
window_mask(struct misaki *c, Picture *win, Picture *mask,
            XWindowAttributes *attr, struct winrule *effects)
{
    *mask = effects->mask;
    if (*mask != None)
    {
        double xs = ((double)effects->mask_w) / (double)attr->width;
        double ys = ((double)effects->mask_h) / (double)attr->height;

        XTransform matrix = {{
            {XDoubleToFixed(xs), XDoubleToFixed(0),  XDoubleToFixed(0)},
            {XDoubleToFixed(0),  XDoubleToFixed(ys), XDoubleToFixed(0)},
            {XDoubleToFixed(0),  XDoubleToFixed(0),  XDoubleToFixed(1.0)}
        }};

        XRenderSetPictureTransform(c->display, *mask, &matrix);
    }

    return STATUS_OK;
}

static enum status
window_image(struct misaki *c, Picture mask, XWindowAttributes *attr,
             Picture im, uint w, uint h)
{
    if (im != None)
    {
        double xs = ((double)w) / (double)attr->width;
        double ys = ((double)h) / (double)attr->height;
        double scale = (xs < ys) ? xs : ys;

        XTransform matrix = {{
            {XDoubleToFixed(scale), XDoubleToFixed(0),  XDoubleToFixed(0)},
            {XDoubleToFixed(0),  XDoubleToFixed(scale), XDoubleToFixed(0)},
            {XDoubleToFixed(0),  XDoubleToFixed(0),  XDoubleToFixed(1.0)}
        }};

        XRenderSetPictureTransform(c->display, im, &matrix);
        XRenderComposite(c->display, PictOpOver, im, mask, c->buffer,
                         ((w / scale) - attr->width)/2,
                         ((h / scale) - attr->height)/2,
                         0, 0,
                         attr->x, attr->y, attr->width, attr->height);
    }

    return STATUS_OK;
}

static enum status
window_precompose(struct misaki *c, Picture *win, Picture *mask,
                  XWindowAttributes *attr, struct winrule *effects)
{
    enum status ret = STATUS_OK;

    if (effects->mask != None)
        ret = window_mask(c, win, mask, attr, effects);
    if (effects->bg != None)
        ret = window_image(c, *mask, attr, effects->bg,
                           effects->bg_w, effects->bg_h);

    return ret;
}

static enum status
window_postcompose(struct misaki *c, Picture win, Picture mask,
                   XWindowAttributes *attr, struct winrule *effects)
{
    enum status ret = STATUS_OK;

    if (effects->fg != None)
        ret = window_image(c, mask, attr, effects->fg,
                           effects->fg_w, effects->fg_h);

    return ret;
}

static enum status
window_compose(struct misaki *c, Window w, XWindowAttributes *attr,
               struct winrule *effects)
{
    enum status ret = STATUS_OK;

    XRenderPictFormat *format = XRenderFindVisualFormat(c->display,
                                                        attr->visual);
    Pixmap wpx = XCompositeNameWindowPixmap(c->display, w);
    if (wpx != None)
    {
        Picture win = XRenderCreatePicture(c->display, wpx, format, 0, NULL);
        if (win != None)
        {
            Picture mask = None;

            ret = window_precompose(c, &win, &mask, attr, effects);

            if (ret == STATUS_OK)
                XRenderComposite(c->display, PictOpOver, win, mask, c->buffer,
                                 0, 0, 0, 0,
                                 attr->x, attr->y, attr->width, attr->height);

            if (ret == STATUS_OK)
                ret = window_postcompose(c, win, mask, attr, effects);

            XRenderFreePicture(c->display, win);
        }
        XFreePixmap(c->display, wpx);
    }

    return ret;
}

/* Misaki functions */

static struct misaki *sc = NULL;
static bool failed = false;

static struct misaki *
misaki_free(struct misaki *c)
{
    if (c != NULL)
    {
        if (c->wallpaper != 0)
            XRenderFreePicture(c->display, c->wallpaper);

        if (c->buffer != 0)
            XRenderFreePicture(c->display, c->buffer);

        if (c->buffer2 != 0)
            XRenderFreePicture(c->display, c->buffer2);

        if (c->windows != NULL)
            window_vec_free(c->windows);

        if (c->line != NULL)
            char_vec_free(c->line);

        if (c->args != NULL)
            charptr_vec_free(c->args);

        if (c->effects.mask != None)
            XRenderFreePicture(c->display, c->effects.mask);

        if (c->name_effects != NULL)
        {
            for (size_t i = 0; i < c->name_effects->size; i++)
            {
                if (c->name_effects->meta[i] <= 0x7f)
                {
                    struct winrule *effects = &(c->name_effects->data[i]);
                    char_vec_free(effects->id);
                    if (effects->mask != None)
                        XFreePixmap(c->display, effects->mask);
                    if (effects->bg != None)
                        XFreePixmap(c->display, effects->bg);
                    if (effects->bgm != None)
                        XFreePixmap(c->display, effects->bgm);
                }
            }

            winrule_map_free(c->name_effects);
        }

        if (c->class_effects != NULL)
        {
            for (size_t i = 0; i < c->class_effects->size; i++)
            {
                if (c->class_effects->meta[i] <= 0x7f)
                {
                    struct winrule *effects = &(c->class_effects->data[i]);
                    char_vec_free(effects->id);
                    if (effects->mask != None)
                        XFreePixmap(c->display, effects->mask);
                    if (effects->bg != None)
                        XFreePixmap(c->display, effects->bg);
                    if (effects->bgm != None)
                        XFreePixmap(c->display, effects->bgm);
                }
            }

            winrule_map_free(c->class_effects);
        }

        if (c->display != NULL)
            XCloseDisplay(c->display);

        free(c);
    }

    return NULL;
}

static struct misaki *
misaki_fail(struct misaki *c, char *string)
{
    error(string);
    return misaki_free(c);
}

static struct misaki *
misaki_init(void)
{
    struct misaki *c = calloc(1, sizeof(struct misaki));
    sc = c;

    /* Defining variables */

    c->display = XOpenDisplay(NULL);
    if (c->display == NULL)
        c = misaki_fail(c, "Failed to open display");

    if (c != NULL)
    {
        c->screen = DefaultScreen(c->display);
        c->root = DefaultRootWindow(c->display);

        c->visual = DefaultVisual(c->display, c->screen);
        c->format = XRenderFindVisualFormat(c->display, c->visual);

        c->rgb = XRenderFindStandardFormat(c->display, PictStandardRGB24);
        c->a1 = XRenderFindStandardFormat(c->display, PictStandardA1);

        c->width = DisplayWidth(c->display, c->screen);
        c->height = DisplayHeight(c->display, c->screen);
        c->depth = DefaultDepth(c->display, c->screen);

        c->windows = window_vec_alloc(32);
        c->line = char_vec_alloc(128);
        c->args = charptr_vec_alloc(16);

        c->name_effects = winrule_map_alloc(16);
        c->class_effects = winrule_map_alloc(16);

        if (pipe(c->stopper) < 0 || c->format == NULL ||
            c->rgb == NULL || c->a1 == NULL ||
            c->windows == NULL || c->line == NULL || c->args == NULL ||
            c->name_effects == NULL || c->class_effects == NULL)
        {
            c = misaki_fail(c, "Out of memory");
        }
        else
        {
            fcntl(c->stopper[0], F_SETFL, O_NONBLOCK);
            fcntl(c->stopper[1], F_SETFL, O_NONBLOCK);
        }
    }

    if (c != NULL)
        XGrabServer(c->display);

    /* Checks */
    if (c != NULL)
    {
        if (!XShapeQueryExtension(c->display, &(c->xshape_ev),
                                  &(c->xshape_err)))
        {
            c = misaki_fail(c, "No XShape extension");
        }
    }

    if (c != NULL)
    {
        if (!XDamageQueryExtension(c->display, &(c->xdamage_ev),
                                   &(c->xdamage_err)))
        {
            c = misaki_fail(c, "No XDamage extension");
        }
    }

    if (c != NULL)
    {
        if (!XRenderQueryExtension(c->display, &(c->xrender_ev),
                                   &(c->xrender_err)))
        {
            c = misaki_fail(c, "No XRender extension");
        }
    }

    if (c != NULL)
    {
        if (!XQueryExtension(c->display, COMPOSITE_NAME, &(c->xcomposite_op),
                             &(c->xcomposite_ev), &c->xcomposite_err))
        {
            c = misaki_fail(c, "No XComposite extension");
        }
    }

    /* Setting events */
    if (c != NULL)
    {
        XCompositeRedirectSubwindows(c->display, c->root,
                                     CompositeRedirectManual);
        XSelectInput(c->display, c->root, ExposureMask |
                                          PropertyChangeMask |
                                          StructureNotifyMask |
                                          SubstructureNotifyMask);

        uint count = 0;
        Window root = {0}, parent = {0}, *children = NULL;
        XShapeSelectInput(c->display, c->root, ShapeNotifyMask);
        XQueryTree(c->display, c->root, &root, &parent, &children, &count);
        if (children != NULL)
        {
            for (uint i = 0; i < count; i++)
            {
                XWindowAttributes attr = {0};
                XGetWindowAttributes(c->display, children[i], &attr);

                if (attr.class != InputOnly)
                {
                    XDamageCreate(c->display, children[i],
                                  XDamageReportNonEmpty);
                }

                if (!window_vec_append(c->windows, &(children[i])))
                    c = misaki_fail(c, "Out of memory");
            }
            XFree(children);
        }
        else
        {
            c = misaki_fail(c, "XQueryTree returned empty");
        }
    }

    /* Setting buffers */
    if (c != NULL)
    {
        Pixmap wpp = XCreatePixmap(c->display, c->root,
                                   c->width, c->height, c->depth);
        Pixmap rpx = XCreatePixmap(c->display, c->root,
                                   c->width, c->height, c->depth);

        if (wpp != 0 && rpx != 0)
        {
            c->wallpaper = XRenderCreatePicture(c->display, wpp, c->format,
                                                0, NULL);
            XFreePixmap(c->display, wpp);

            c->buffer = XRenderCreatePicture(c->display, rpx, c->format,
                                             0, NULL);
            XFreePixmap(c->display, rpx);

            XRenderPictureAttributes pa = {0};
            pa.subwindow_mode = IncludeInferiors;
            c->buffer2 = XRenderCreatePicture(c->display, c->root, c->format,
                                              CPSubwindowMode, &pa);

            if (c->wallpaper == 0 || c->buffer == 0 || c->buffer2 == 0)
                c = misaki_fail(c, "Failed to create Pictures");
        }
        else
        {
            c = misaki_fail(c, "Failed to create Pixmaps");
        }
    }

    if (c != NULL)
    {
        XRenderComposite(c->display, PictOpSrc, c->buffer2, None, c->wallpaper,
                        0, 0, 0, 0,
                        0, 0, c->width, c->height);
    }

    XUngrabServer(c->display);

    return c;
}

static struct misaki *
misaki_register(struct misaki *c)
{
    char prop[] = "_NET_WM_CM_Sxxxxxxxxxx";

    snprintf(prop, sizeof(prop), "_NET_WM_CM_S%d", c->screen);
    Atom a = XInternAtom(c->display, prop, False);

    Window w = XGetSelectionOwner(c->display, a);
    if (!failed && w != None)
    {
        XTextProperty xtp = {0};

        Atom wm = XInternAtom(c->display, "_NET_WM_NAME", False);
        if (!XGetTextProperty(c->display, w, &xtp, wm) &&
            !XGetTextProperty(c->display, w, &xtp, XA_WM_NAME))
        {
            c = misaki_fail(c, "Another compositor is already running");
            failed = true;
        }

        if (!failed)
        {
            char **strings = NULL;
            int string_c = 0;
            if (XmbTextPropertyToTextList(c->display, &xtp, &strings,
                                          &string_c) == Success)
            {
                c = misaki_fail(c, "Another compositor is already running");
                failed = true;

            }
            XFreeStringList(strings);
        }
        XFree(xtp.value);
    }

    return c;
}

static enum status
misaki_render(struct misaki *c)
{
    enum status ret = STATUS_OK;

    XRenderComposite(c->display, PictOpSrc, c->wallpaper, None, c->buffer,
                     0, 0, 0, 0, 0, 0, c->width, c->height);

    uint count = 0;
    Window root = {0}, parent = {0}, *children = NULL;
    XShapeSelectInput(c->display, c->root, ShapeNotifyMask);
    XQueryTree(c->display, c->root, &root, &parent, &children, &count);
    for (uint i = 0; i < count; i++)
    {
        XWindowAttributes attr = {0};
        XGetWindowAttributes(c->display, children[i], &attr);

        if (attr.map_state & IsViewable)
        {
            struct winrule *name_effect = NULL;
            struct winrule *class_effect = NULL;

            XClassHint hint = {0};
            if (XGetClassHint(c->display, children[i], &hint) != 0)
            {
                struct char_vec_t nid = {0};
                struct char_vec_t cid = {0};
                struct winrule nquery = {.id = &nid};
                struct winrule cquery = {.id = &cid};
                nquery.id->data = hint.res_name;
                nquery.id->count = strlen(hint.res_name) + 1;
                cquery.id->data = hint.res_class;
                cquery.id->count = strlen(hint.res_class) + 1;

                name_effect = winrule_map_search(c->name_effects, &nquery);
                class_effect = winrule_map_search(c->class_effects, &cquery);

                XFree(hint.res_name);
                XFree(hint.res_class);
            }

            if (name_effect != NULL)
            {
                ret = window_compose(c, children[i], &attr, name_effect);
            }
            else if (class_effect != NULL)
            {
                ret = window_compose(c, children[i], &attr, class_effect);
            }
            else
            {
                ret = window_compose(c, children[i], &attr, &(c->effects));
            }
        }
    }
    XFree(children);

    XRenderComposite(c->display, PictOpSrc, c->buffer, None, c->buffer2,
                     0, 0, 0, 0, 0, 0, c->width, c->height);

    return ret;
}

static enum status
misaki_step(struct misaki *c)
{
    enum status ret = STATUS_OK;

    XEvent e = {0};
    XNextEvent(c->display, &e);
    if (e.type == CreateNotify)
    {
        XCreateWindowEvent ev = e.xcreatewindow;

        if (ev.window != 0)
        {
            XWindowAttributes attr = {0};
            XGetWindowAttributes(c->display, ev.window, &attr);

            if (attr.class != InputOnly)
            {
                XDamageCreate(c->display, ev.window,
                              XDamageReportNonEmpty);
            }

            if (!window_vec_append(c->windows, &(ev.window)))
                ret = error("Out of memory");
        }
    }
    else if (e.type == DestroyNotify)
    {
        XDestroyWindowEvent ev = e.xdestroywindow;

        if (ev.window != 0)
        {
            Window *item = window_vec_find(c->windows, &(ev.window));
            if (item != NULL)
                window_vec_delete(c->windows, item);
        }
    }
    else if (e.type == (c->xdamage_ev + XDamageNotify))
    {
        XDamageNotifyEvent *ev = (XDamageNotifyEvent*)&e;

        if (ev->drawable != 0)
        {
            if (window_vec_find(c->windows, &(ev->drawable)) != NULL)
            {
                ret = misaki_render(c);

                XWindowAttributes attr = {0};
                XGetWindowAttributes(c->display, ev->drawable, &attr);
                XDamageSubtract(c->display, ev->damage, None, None);
            }
        }
    }
    else
    {
        Atom setroot = XInternAtom(c->display, "ESETROOT_PMAP_ID", True);
        if (e.type == PropertyNotify && e.xproperty.atom == setroot)
        {
            c->root = e.xproperty.window;

            Pixmap wpp = XCreatePixmap(c->display, c->root,
                                       c->width, c->height, c->depth);
            if (wpp != 0)
            {
                Picture new = XRenderCreatePicture(c->display, wpp,
                                                   c->format, 0, NULL);
                if (new != 0)
                {
                    XRenderComposite(c->display, PictOpSrc,
                                     new, None, c->wallpaper,
                                     0, 0, 0, 0,
                                     0, 0, c->width, c->height);
                    XRenderFreePicture(c->display, new);
                }
                XFreePixmap(c->display, wpp);
            }
        }

        ret = misaki_render(c);
    }

    if (failed)
        ret = STATUS_ERROR;

    return ret;
}

static enum status command_run(struct misaki *c);
static enum status
misaki_run(struct misaki *c)
{
    enum status ret = STATUS_OK;

    while (XPending(c->display))
        ret = misaki_step(c);

    while (ret == STATUS_OK && !failed)
    {
        int xfd = ConnectionNumber(c->display);

        fd_set fd_arr = {0};
        FD_ZERO(&fd_arr);
        FD_SET(c->stopper[0], &fd_arr);
        FD_SET(xfd, &fd_arr);
        FD_SET(STDIN_FILENO, &fd_arr);

        int max = (xfd > c->stopper[0]) ? xfd : c->stopper[0];
        max = (max > STDIN_FILENO) ? max : STDIN_FILENO;

        struct timeval tv = {0, 1000000/60};
        int result = select(max + 1, &fd_arr, NULL, NULL, &tv);
        if (result < 0 && errno != EINTR && errno != EAGAIN)
            ret = error("Failed select");

        if (ret == STATUS_OK && FD_ISSET(c->stopper[0], &fd_arr))
            break;

        if (ret == STATUS_OK && FD_ISSET(xfd, &fd_arr))
        {
            while (XPending(c->display))
                ret = misaki_step(c);
        }

        if (ret == STATUS_OK && FD_ISSET(STDIN_FILENO, &fd_arr))
            ret = command_run(c);

        if (ret == STATUS_OK)
            ret = misaki_render(c);
    }

    return ret;
}

/* Command functions */

static enum status
command_return(struct misaki *c)
{
    enum status ret = STATUS_OK;

    if (c->socket != NULL && c->response != NULL)
    {
        int fd[2];
        if (pipe(fd) < 0)
            return STATUS_ERROR;

        if (ret == STATUS_OK)
        {
            pid_t pid = fork();
            if (pid > 0)
            {
                close(fd[0]);
                write(fd[1], c->response, strlen(c->response));
                write(fd[1], "\n", 1);
                close(fd[1]);
            }
            else if (pid == 0)
            {
                close(fd[1]);
                dup2(fd[0], STDIN_FILENO);

                char *args[] = {"misaka", "connect", c->socket, c->caller,
                                "xmtr", NULL};
                execvp(args[0], args);
                close(fd[0]);
            }
            else
            {
                ret = STATUS_ERROR;
            }
        }
    }

    return ret;
}

static enum status
command_window_mask(struct misaki *c, struct winrule *effects)
{
    enum status ret = STATUS_OK;

    if (effects->mask != None)
    {
        XRenderFreePicture(c->display, effects->mask);
        effects->mask = None;
    }

    char *file = c->args->data[c->argidx++];
    if (file != NULL)
    {
        int x = 0, y = 0;
        uint w = 0, h = 0;

        Pixmap px = None;
        if (XReadBitmapFile(c->display, c->root, file,
                            &w, &h, &px, &x, &y) == BitmapSuccess)
        {
            effects->mask = XRenderCreatePicture(c->display, px,
                                                 c->a1, 0, NULL);
            effects->mask_w = w;
            effects->mask_h = h;
            XFreePixmap(c->display, px);

            c->response = "SUCCESS: Mask applied";
        }
        else
        {
            c->response = "FAILED: Couldn't open file";
        }
    }
    else
    {
        c->response = "FAILED: Lacking filename";
    }

    return ret;
}

static enum status
command_window_image(struct misaki *c,
                     Picture *im, Picture *mask, uint *w, uint *h,
                     char *success)
{
    enum status ret = STATUS_OK;

    if (*im != None)
    {
        XRenderFreePicture(c->display, *im);
        *im = None;
    }

    char *file = c->args->data[c->argidx++];
    if (file != NULL)
    {
        Pixmap impx = None;
        Pixmap maskpx = None;

        XpmAttributes attr = {0};
        if (XpmReadFileToPixmap(c->display, c->root, file,
            &impx, &maskpx, &attr) == XpmSuccess)
        {
            *mask = None;
            if (maskpx != None)
            {
                *mask = XRenderCreatePicture(c->display, maskpx, c->a1,
                                             0, NULL);
            }

            if (*mask != None)
            {
                XRenderPictureAttributes pattrs = {.alpha_map = *mask};
                *im = XRenderCreatePicture(c->display, impx, c->rgb,
                                           CPAlphaMap, &pattrs);
            }
            else
            {
                *im = XRenderCreatePicture(c->display, impx, c->rgb, 0, NULL);
            }

            *w = attr.width;
            *h = attr.height;
            XpmFreeAttributes(&attr);
            c->response = success;
        }
        else
        {
            c->response = "FAILED: Couldn't open file";
        }
    }
    else
    {
        c->response = "FAILED: Lacking filename";
    }

    return ret;
}

static enum status
command_window_switch(struct misaki *c, struct winrule *effects)
{
    enum status ret = STATUS_OK;

    char *command = c->args->data[c->argidx++];
    if (command != NULL)
    {
        if (pstrcmp(command, "mask") == 0)
        {
            ret = command_window_mask(c, effects);
        }
        else if (pstrcmp(command, "bg") == 0)
        {
            ret = command_window_image(c, &(effects->bg), &(effects->bgm),
                                       &(effects->bg_w), &(effects->bg_h),
                                       "SUCCESS: Background applied");
        }
        else if (pstrcmp(command, "fg") == 0)
        {
            ret = command_window_image(c, &(effects->fg), &(effects->fgm),
                                       &(effects->fg_w), &(effects->fg_h),
                                       "SUCCESS: Foreground applied");
        }
        else
        {
            c->response = "FAILED: Unknown effect";
        }
    }

    return ret;
}

static enum status
command_window(struct misaki *c)
{
    enum status ret = STATUS_OK;

    char *command = c->args->data[c->argidx++];
    if (command != NULL)
    {
        struct winrule *r = NULL;
        if (pstrcmp(command, "any") == 0)
        {
            r = &(c->effects);
        }
        else if (pstrcmp(command, "name") == 0 ||
                 pstrcmp(command, "class") == 0)
        {
            struct winrule_map_t *map = (command[0] == 'n') ? c->name_effects :
                                                              c->class_effects;

            char *string = c->args->data[c->argidx++];
            size_t length = strlen(string);
            if (string != NULL)
            {
                struct char_vec_t id = {.data = string, .count = length + 1};
                struct winrule query = {.id = &id};
                r = winrule_map_search(map, &query);
                if (r == NULL)
                {
                    if (winrule_map_insert(map, &query))
                    {
                        r = winrule_map_search(map, &query);
                        if (r != NULL)
                        {
                            r->id = char_vec_alloc(length + 1);
                            if (r->id != NULL)
                            {
                                r->id->count = length + 1;
                                memcpy(r->id->data, string, r->id->count);
                            }
                            else
                            {
                                ret = error("Out of memory");
                            }
                        }
                    }
                }

                if (r == NULL)
                    ret = error("Out of memory");
            }
            else
            {
                c->response = "FAILED: Lacking name/class";
            }
        }
        else
        {
            c->response = "FAILED: Unknown selection";
        }

        if (ret == STATUS_OK && r != NULL)
            ret = command_window_switch(c, r);
    }
    else
    {
        c->response = "FAILED: Lacking selection";
    }

    return ret;
}

static enum status
command_parse(struct misaki *c)
{
    enum status ret = STATUS_OK;

    if (ret == STATUS_OK)
    {
        c->caller = c->line->data;

        c->args->count = 0;
        for (size_t pos = strlen(c->caller) + 1; pos < c->line->count;)
        {
            char *arg = &(c->line->data[pos]);
            charptr_vec_append(c->args, &arg);
            pos += strlen(arg) + 1;
        };
        c->args->data[c->args->count] = NULL;

        c->argidx = 0;
        if (pstrcmp(c->args->data[c->argidx++], "window") == 0)
        {
            ret = command_window(c);
        }
        else
        {
            c->response = "FAILED: Unknown command";
        }
    }

    return ret;
}

static enum status
command_run(struct misaki *c)
{
    enum status ret = STATUS_OK;

    c->line->count = 0;

    int ch = '?';
    while (1)
    {
        ch = fgetc(stdin);
        if (ch == EOF)
            break;

        char cc = ch;
        if (!char_vec_append(c->line, &cc))
            ret = error("No memory");

        char ch2 = fgetc(stdin);
        if (ch == '\0' && ch2 == '\0')
            break;
        ungetc(ch2, stdin);
    }

    c->response = NULL;
    if (ret == STATUS_OK && (c->line->count > 1 || ch != EOF))
        ret = command_parse(c);

    if (ret == STATUS_OK)
        ret = misaki_render(c);

    if (ret == STATUS_OK)
        ret = command_return(c);

    return ret;
}

/* Main functions */

static int
xerror(Display *display, XErrorEvent *ev)
{
    if (sc != NULL)
    {
        if (ev->request_code == sc->xcomposite_op &&
            ev->minor_code == X_CompositeRedirectSubwindows)
        {
            error("Another compositor is already running");
            failed = true;
        }
    }

    #ifndef NDEBUG
    if (!failed)
    {
        char buffer[1024] = {0};

        XGetErrorText(display, ev->error_code, buffer, sizeof(buffer));
        fprintf(stderr, "XErrorEvent: serial=%ld error=%d "
                        "request=%d minor=%d: %s\n",
                ev->serial, ev->error_code, ev->request_code, ev->minor_code,
                buffer);
    }
    #endif

    return 0;
}

static void
sighandler(int code)
{
    failed = true;

    char dummy = '\0';
    write(sc->stopper[1], &dummy, 1);
}

extern int
main(int argc, char *argv[])
{
    enum status ret = STATUS_OK;

    if (argc <= 2)
    {
        XSetErrorHandler(xerror);
        signal(SIGINT, sighandler);
        signal(SIGTERM, sighandler);

        struct misaki *c = misaki_init();
        if (argc > 1)
            c->socket = argv[1];

        if (c != NULL)
            c = misaki_register(c);

        if (c != NULL)
        {
            ret = misaki_run(c);
            misaki_free(c);
        }
        else
        {
            ret = STATUS_ERROR;
        }
    }
    else
    {
        fprintf(stderr, "misaki [SOCKET]\n");
        fprintf(stderr, "X11 compositor\n");
        ret = STATUS_ERROR;
    }

    return ret;
}
